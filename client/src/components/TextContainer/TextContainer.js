import React from 'react';

import onlineIcon from '../../icons/onlineIcon.png';

import './TextContainer.css';

const TextContainer = ({ users,room }) => (
  <div className="textContainer">
    <div>
      <h1>Realtime Chat Application Demo</h1>
    </div>
    {
      users
        ? (
          <div className="groupInfo">
            <h2>Group Name : {room}</h2>
            <h3>People currently chatting:</h3>
            <div className="activeContainer">
              <h2>
                {users.map(({name}) => (
                  <div key={name} className="activeItem">
                    {name}
                    <img alt="Online Icon" src={onlineIcon}/>
                  </div>
                ))}
              </h2>
            </div>
          </div>
        )
        : null
    }
  </div>
);

export default TextContainer;