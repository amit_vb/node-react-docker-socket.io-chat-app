# Node-react-docker-socket.io-chat-app

## using docker

### install docker and docker compose in your system

#### Start

> docker-compose up -d

#### Stop

> docker-compose down

#### Restart

> docker-compose restart

> visit localhots:3000

## without docker

### run node app

> cd server

> npm i

> npm start

### runn react app

> cd client

> npm i

> npm start

> visit localhots:3000
